
const $bgVideo = document.querySelector('.bg-video'),
$playVideo = document.querySelector('.play-video'),
$videoSpan = document.querySelector('.video-span'),
$sectionVideoFrame = document.querySelector('.section-video iframe');

$bgVideo.addEventListener('click', () => {
    $playVideo.style.display = 'none';
    $videoSpan.style.display = 'none';
    $bgVideo.style.display = 'none';
    $sectionVideoFrame.style.display = 'block';
});